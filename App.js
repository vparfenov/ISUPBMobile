import React from 'react';
import Navigation from './src/components/Navigation';
import { AuthProvider } from './src/context/AuthContext';
import { TrainingProgramProvider } from './src/context/TrainingProgramContext';

const App = () => {
  return (
    <AuthProvider>
        <TrainingProgramProvider>
            <Navigation/>
        </TrainingProgramProvider>
    </AuthProvider>
     
  );
};

export default App;
