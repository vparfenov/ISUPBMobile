import React, { useContext } from 'react';
import { Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen';
import LoginScreen from '../screens/LoginScreen';
import SettingScreen from '../screens/SettingScreen';
import EventScreen from '../screens/EventScreen';
import ModuleScreen from '../screens/ModuleScreen';
import TestScreen from '../screens/TestScreen';
import WorkPermitScreen from '../screens/WorkPermitScreen';
import RevisionScreen from '../screens/RevisionScreen';
import CreateScreen from '../screens/CreateScreen';
import SafetyTalkScreen from '../screens/SafetyTalkScreen';
import AuditScreen from '../screens/AuditScreen';
import FindingScreen from '../screens/FindingScreen';
import { AuthContext } from '../context/AuthContext';
import {IsSLK} from '../config';


const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const Navigation = () => {

    const {isLoading, userInfo, login} = useContext(AuthContext);  

    return (
        <NavigationContainer>
            <Tab.Navigator screenOptions={({ route }) => ({
                    tabBarIcon: ({ focused, color, size }) => {
                        let iconName;
            
                        if (route.name === 'Event') {
                          iconName = focused ? 'calendar' : 'calendar-outline';
                        } else if (route.name === 'Analytics') {
                          iconName = focused ? 'bar-chart' : 'bar-chart-outline';
                        } else if (route.name === 'Create') {
                          iconName = focused ? 'create' : 'create-outline';
                        } else if (route.name === 'Settings' || route.name === 'Login') {
                          iconName = focused ? 'settings' : 'settings-outline';
                        }
                      
                        return <Ionicons name={iconName} size={size} color={color} />;
                      },
                      tabBarActiveTintColor: 'tomato',
                      tabBarInactiveTintColor: 'gray',
                      tabBarShowLabel: false,
                      headerShown: false,
                })} initialRouteName='Event'>
    
                {userInfo && userInfo.Id ? (
                    <>
                        <Tab.Screen name="Event">
                            {() => (
                                <Stack.Navigator>
                                <Stack.Screen 
                                  name="Home" 
                                  component={EventScreen}
                                  options={{
                                    title: 'Органайзер',
                                    headerShown: false 
                                  }}
                                />
                                <Stack.Screen 
                                  name="Module" 
                                  component={ModuleScreen} 
                                  options={({ route }) => ({ title: route.params.title })}
                                />
                                <Stack.Screen 
                                  name="Test" 
                                  component={TestScreen}
                                  options={({ route }) => ({ title: route.params.title })}
                                />
                                 <Stack.Screen 
                                  name="WorkPermit" 
                                  component={WorkPermitScreen}
                                  options={{
                                    title: 'Наряд-допуск',
                                  }} 
                                />
                                 <Stack.Screen 
                                  name="Revision" 
                                  component={RevisionScreen}
                                  options={{
                                    title: 'На доработку',
                                  }} 
                                />
                              </Stack.Navigator>
                            )} 
                        </Tab.Screen>
                        {IsSLK ? (
                          <Tab.Screen name="Create">
                          {() => (
                              <Stack.Navigator>
                                  <Stack.Screen 
                                      name="Home" 
                                      component={CreateScreen}
                                      options={{
                                        title: 'Создать',
                                        headerShown: false 
                                      }}
                                  />
                                  <Stack.Screen 
                                      name="SafetyTalk" 
                                      component={SafetyTalkScreen}
                                      options={{
                                          title: 'Беседа по безопасности',
                                      }} 
                                  />
                                  <Stack.Screen 
                                      name="Audit" 
                                      component={AuditScreen}
                                      options={{
                                          title: 'Аудит',
                                      }} 
                                  />
                                  <Stack.Screen 
                                      name="Finding" 
                                      component={FindingScreen}
                                      options={{
                                          title: 'Несоответствие',
                                      }} 
                                  />
                              </Stack.Navigator>
                          )}
                          </Tab.Screen>    
                        ) : (
                           <></> 
                        )}
                        
                        <Tab.Screen name="Settings" component={SettingScreen}>
                        </Tab.Screen> 
                    </>
                ) : (
                    <Tab.Screen name="Login" component={LoginScreen}>
                    </Tab.Screen>     
                )}
            </Tab.Navigator>
        </NavigationContainer>
    );
};

export default Navigation;