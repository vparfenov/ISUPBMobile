import React, {useContext} from 'react';
import {View, Text, Button, StyleSheet, FlatList, ScrollView} from 'react-native';
import { AuthContext } from '../context/AuthContext';
import Spinner from 'react-native-loading-spinner-overlay';

const SettingScreen = () => {
    
    const {isLoading, userInfo, login, logout} = useContext(AuthContext);    

    return (

        <FlatList
            data={userInfo.Enterprises}
            renderItem={({item}) => (
                <View style={styles.item}>
                    <Text style = {styles.title}>{item.EnterpriseName} </Text>
                    <Text style = {styles.itemText}>Роль: {item.RoleName}</Text>
                    <Text style = {styles.itemText}>Подразделения: {item.DepartmentsName}</Text>
                    <Text style = {styles.itemText}>Сотрудник: {item.EmployeeFullName}</Text>
                    <Text style = {styles.itemText}>Подрядчик: {item.CounterpartyName}</Text>
                </View>
            )}
            keyExtractor={item => item.EnterpriseId}     
            ListHeaderComponent={() =>
                <View style={styles.wrapperSetting}> 
                    <Text style = {styles.userText}>{userInfo.FullName}</Text>
                    <Text style = {styles.settingText}>{userInfo.Position} </Text>
                </View> 
            }
            ListFooterComponent={() =>
                <View style={styles.wrapperSetting}>
                    <Button 
                        title='Выйти'
                        onPress={() => {
                            logout()
                        }}
                    />
                </View>      
            }     
        />

           
    );
};

const styles = StyleSheet.create({
    wrapperSetting: {
      padding: 20,
      width: '80%',
      alignItems: 'center',
      justifyContent: 'center'
    },
    settingText: {
        fontSize: 16,
        color: 'black',
        textAlign: 'center'
    },
    userText: {
        fontSize: 18,
        color: '#428bca',
        textAlign: 'center'
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
        fontSize: 18,
        color: '#428bca'
    },
    itemText: {
        fontSize: 16,
        color: 'black',
        textAlign: 'left'
    }
  });

export default SettingScreen;