import React from 'react';
import { Text, View, SafeAreaView, Button, ScrollView, StyleSheet, StatusBar} from 'react-native';

const WorkPermitScreen = () => {
    return (
        <SafeAreaView>
    <ScrollView>
      <Text style={styles.title1}>
        1.1. Производителю работ
      </Text>
      <Text style={styles.detail1}>
        Электрогазосварщик 4 разряд, Филиал "Сухоложскцемент", электромеханический цех, Быков О. В. 
      </Text>
      <Text style={styles.title1}>
        с бригадой в составе 1 человек поручается произвести следующие работы:
      </Text>
      <Text style={styles.detail1}>
      Резка рельс в металлолом под погрузку., локомотивное депо 
      </Text>
      <Text style={styles.title1}>
        1.2. Опасные и вредные производственные факторы, которые действуют или могут возникнуть в местах выполнения работ:
      </Text>
      <Text style={styles.detail1}>
        1 Пыль
      </Text>
      <Text style={styles.detail1}>
       2 Ручной инструмент
      </Text>
      <Text style={styles.detail1}>
      3 Электрогазосварочные работы 
      </Text>
      <Text style={styles.title1}>
      1.3. Выбор систем безопасности:
      </Text>
      <Text style={styles.detail1}>
      Средства индивидуальной защиты, в том числе СИЗОД (фильтрующие или изолирующие)
      </Text>
      <Text style={styles.detail1}>
      Автономные изолирующие средства индивидуальной защиты органов дыхания с внешней подачей воздуха для дыхания или без таковой (далее - ИСЗОД)
      </Text>
      <Text style={styles.detail1}>
      Удерживающие системы
      </Text>
      <Text style={styles.detail1}>
      Системы позиционирования
      </Text>
      <Text style={styles.detail1}>
      Страховочные системы
      </Text>
      <Text style={styles.detail1}>
      Самоспасатели и системы эвакуации и спасения
      </Text>
      <Text style={styles.title1}>
      1.4. Необходимые для производства работ:
      </Text>
      <Text style={styles.detail1}>
      материалы:
      </Text>
      <Text style={styles.detail1}>
      инструменты:
      </Text>
      <Text style={styles.detail1}>
      приспособления:
      </Text>
      <Text style={styles.title1}>
      2. При подготовке и производстве работ обеспечить следующие меры безопасности (указываются организационные и технические меры безопасности, осуществляемые при подготовке места проведения работ, в том числе по пожарной безопасности при проведении огневых работ):
      </Text>
      <Text style={styles.detail1}>
      Применить защитные очки, до начала и во время работ, Быков О. В.
      </Text>
      <Text style={styles.title1}>
      3. Начать работы:
      </Text>
      <Text style={styles.detail1}>
      в 12-30 28.12.2023 г.
      </Text>
      <Text style={styles.title1}>
      4. Окончить работы: 
      </Text>
      <Text style={styles.detail1}>
      в 15-30 28.12.2023 г.
      </Text>
      <Text style={styles.title1}>
      5. Наряд выдал руководитель работ
      </Text>
      <Text style={styles.detail1}>
      Мастер (по ремонту пути), Потапов В. В.
      </Text>
      <Text style={styles.title1}>
      6. С условиями работы ознакомлены:
      </Text>
      <Text style={styles.detail1}>
      Мастер (по ремонту пути), Потапов В. В.
      </Text>
      <Text style={styles.title1}>
      7. Инструктаж по охране труда в объеме:
      </Text>
      <Text style={styles.detail1}>
      Ноговицын О. В., Слесарь-ремонтник 4 разряда 
      </Text>
      <Text style={styles.title1}>
      8. Разрешение на вход в ОЗП после подтверждения соответствующих блокировок в ОЗП и замеров сопутствующих газов в ОЗП выдал:
      </Text>
      <Text style={styles.detail1}>
      Руководитель работ: Работы в ОЗП не производятся
      </Text>
      <Text style={styles.title1}>
      Разрешение на вход в ОЗП после подтверждения соответствующих блокировок в ОЗП получил:
      </Text>
      <Text style={styles.detail1}>
      Работы в ОЗП не производятся
      </Text>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{flex: 0.5}}>
                <Button title="Согласовать"/>
            </View>
            <View style={{flex: 0.5}}>
                <Button title="ОТклонить"/>
            </View>
      </View>
      
    </ScrollView>
  </SafeAreaView>

    );
};

const styles = StyleSheet.create({
    containerLogin: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    wrapperLogin: {
      width: '80%'
    },
    inputLogin: {
      marginBottom: 12,
      borderWidth: 1,
      borderColor: '#bbb',
      borderRadius: 5,
      paddingHorizontal: 14
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 12,
        color: 'black'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });

export default WorkPermitScreen;