import React, {useState, useContext, useEffect, useCallback} from 'react';
import {View, Text, Button, StyleSheet, FlatList, RefreshControl , StatusBar, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AuthContext } from '../context/AuthContext';
import { TrainingProgramContext } from '../context/TrainingProgramContext';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import _ from 'lodash';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';


const ModuleScreen = ({ route, navigation }) => {

    const {isLoading, trainingProgram, getTrainingProgram} = useContext(TrainingProgramContext)
    const {userInfo} = useContext(AuthContext); 

    //mode: 1 - test, 2 - education
    const {objectId, mode, title} = route.params;


    useEffect(() => {
        getTrainingProgram(objectId);
    }, [])


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            //console.log('focus');
            onRefresh();
      
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                //console.log('unfocus')
              };
        }, [])
      );
    
    const [needRefresh, setNeedRefresh] = useState(false);

    const wait = (timeout) => { // Defined the timeout function for testing purpose
        return new Promise(resolve => setTimeout(resolve, timeout));
    }    

    const onRefresh = useCallback(() => {
        setNeedRefresh(true);
        wait(10).then(() => setNeedRefresh(false));
    }, []);

    return (
        <FlatList
            data={trainingProgram.Modules}
            refreshing={needRefresh}
            onRefresh={onRefresh}
            renderItem={({item}) => (
                <View style={styles.item}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style = {styles.title}>{item.Name} </Text>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{flex: 0.35}}>
                            <Text style = {styles.settingText}>Общее количество вопросов:</Text>
                        </View>
                        <View style={{flex: 0.10}}>
                            <Text style = {styles.title}>{item.NumberQuestions} </Text>
                        </View>
                        <View style={{flex: 0.35}}>
                            <Text style = {styles.settingText}>% правильных ответов:</Text>
                        </View>
                        <View style={{flex: 0.10}}>
                            <Text style = {styles.title}>{item.CurrentPercentage} </Text>
                        </View>
                        <View style={{flex: 0.10}}>
                            <TouchableOpacity onPress={() => {navigation.navigate('Test', {module: item, mode: mode, title: title}) }} style={{
                                borderWidth:1,
                                borderColor:'rgba(0,0,0,0.2)',
                                alignItems:'center',
                                justifyContent:'center',
                                width:30,
                                height:30,
                                backgroundColor:'#fff',
                                borderRadius:50,
                                }}>
                                <Ionicons name='play' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                    </View>
                   
                    
                </View>
            )}
            keyExtractor={item => item.Id}     
            ListHeaderComponent={() =>
                <View style={styles.wrapperSetting}> 
                    <Text style = {styles.userText}>{trainingProgram.Name}</Text>
                </View> 
            }
            ListFooterComponent={() =>
                <View style={styles.wrapperSetting}>
                    {mode == 1 ? (
                        <Button 
                        title='Завершить'
                        onPress={() => {

                            let testResult = {};

                            testResult.scheduleId = objectId;
                            testResult.startTesting = trainingProgram.StartTesting;

                            _.each(trainingProgram.Modules, module => {
                                
                                testResult[module.Id] = {};
                                testResult[module.Id].count = module.NumberQuestionsTicket;
                                testResult[module.Id].moduleName = module.Name;
                                testResult[module.Id].time = null;
                                testResult[module.Id].percent = module.PercentageCorrectAnswers;
                                testResult[module.Id].usedIds = _(module.TestQuizzes)
                                    .chain()
                                    .map(el => el.Id)
                                    .value();
                                testResult[module.Id].answers = {};

                                _.each(module.TestQuizzes, quizz => {

                                    let answers = _(quizz.Answers)
                                        .chain()        
                                        .filter(el => el.IsSelected)
                                        .map(el => el.Id)
                                        .value()

                                        testResult[module.Id].answers[quizz.Id] = answers; 

                                })
                            })

                            axios
                                .post(`${BASE_URL}/saveTestResult`, testResult, {
                                    withCredentials: false,
                                    headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                                })
                                .then(res => {
                                    navigation.goBack();
                                })
                                .catch(e => {
                                    console.log(e);
                                })

                        }}
                    />
                    ) : (
                        <></>
                    )}
                    
                </View>      
            }     
        />
    )

}


const styles = StyleSheet.create({
    settingText: {
        fontSize: 16,
        color: 'black',
        textAlign: 'center'
    },
    userText: {
        fontSize: 18,
        color: '#428bca',
        textAlign: 'center'
    },
    wrapperSetting: {
        padding: 20,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
      },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,

    },
    title: {
      padding: 0,
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });

export default ModuleScreen;
    