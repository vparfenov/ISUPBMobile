import React, {useState, useContext, useEffect, useCallback}  from 'react';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import { Text, View, Button, StyleSheet, FlatList,  StatusBar, TouchableOpacity, ScrollView, TextInput} from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import { DateTimePickerAndroid } from '@react-native-community/datetimepicker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';
import { AuthContext } from '../context/AuthContext';
import _ from 'lodash';
import moment from 'moment';

const AuditScreen = ({ route, navigation }) => {

    const [audit, setAudit] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const {userInfo} = useContext(AuthContext);  

    useEffect(() => {
        getAudit();
    }, [])

    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            //console.log('focus');
            onRefresh();
      
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                //console.log('unfocus')
              };
        }, [])
    );

    const [needRefresh, setNeedRefresh] = useState(false);  

    const wait = (timeout) => { // Defined the timeout function for testing purpose
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const onRefresh = useCallback(() => {
        setNeedRefresh(true);
        wait(10).then(() => setNeedRefresh(false));
    }, []);


    const getAudit = () => {

        setIsLoading(true)

        return axios
            .get(`${BASE_URL}/getAudit`, {
                params: {
                    userId: userInfo.Id,
                },
                withCredentials: false,
                headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
            })
            .then(res => {
                setAudit(res.data);
                setIsLoading(false);

            })
            .catch(e => {
                console.log(e);
                setIsLoading(false);
            })
    }

    const [auditContents, setAuditContents] = useState([]);

    const [auditTypeId, setAuditTypeId] = useState(null);
    const [auditLevelId, setAuditLevelId] = useState(null);
    const [departmentId, setDepartmentId] = useState(null);    
    const [auditLevels, setAuditLevels] = useState([]);
    const [date, setDate] = useState(moment().toDate());


    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate;
        setDate(currentDate);
      };
    
    const showMode = (currentMode) => {
        DateTimePickerAndroid.open({
          value: date,
          onChange,
          mode: currentMode,
          is24Hour: true,
          locale: "ru-RU"
        });
    };
    
    const showDatepicker = () => {
        showMode('date');
    };


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            //console.log('focus');
            onRefresh();
      
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                //console.log('unfocus')
              };
        }, [])
      );

    return (
        <FlatList
            data={auditContents}
            refreshing={needRefresh}
            onRefresh={onRefresh}
            renderItem={({item}) => (
                <View style={styles.item}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{flex: 0.75}}>
                            <Text style={styles.detail}>{item.content}</Text>
                        </View>
                        <View style={{flex: 0.125}}>
                            <TouchableOpacity style={styles.actions} onPress={() => {
                                navigation.navigate('Finding', {array: auditContents, id: item.id})
                            }}>
                                <Ionicons name='pencil' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 0.125}}>
                            <TouchableOpacity style={styles.actions} onPress={() => {
                                setAuditContents(_.filter(auditContents, el => el.id != item.id))
                            }}>
                                <Ionicons name='trash' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            )}
            keyExtractor={item => item.id}     
            ListHeaderComponent={() =>
                <View> 
                    <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                        <Text style = {styles.userText}>Параметры аудита</Text>
                    </View>
                    <View style={{ marginHorizontal: 10}}>
                        <Text style={styles.detail}>Тип аудита:</Text>
                    </View>
                    <View style={{}}>
                        <Dropdown
                            style={styles.dropdown}
                            placeholderStyle={styles.placeholderStyle}
                            selectedTextStyle={styles.selectedTextStyle}
                            inputSearchStyle={styles.inputSearchStyle}
                            itemTextStyle={styles.inputSearchStyle}
                            iconStyle={styles.iconStyle}
                            data={audit.auditTypes &&  audit.auditTypes.length > 0 ? audit.auditTypes : []}
                            search
                            maxHeight={300}
                            labelField="label"
                            valueField="value"
                            placeholder="Выберите значение"
                            searchPlaceholder="Поиск..."
                            value={auditTypeId}
                            searchField="label"
                            onChange={item => {
                                setAuditTypeId(item.value);
                                if (item.levels && item.levels.length > 0) {
                                    setAuditLevels(item.levels);
                                } else {
                                    setAuditLevels([]);
                                    setAuditLevelId(null);
                                }
                            }}
                        />
                    </View>
                    {auditLevels && auditLevels.length > 0 ? (
                        <>
                            <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                                <Text style={styles.detail}>Уровень аудита:</Text>
                            </View>
                            <View style={{padding: 5,}}>
                                <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={styles.selectedTextStyle}
                                    inputSearchStyle={styles.inputSearchStyle}
                                    itemTextStyle={styles.inputSearchStyle}
                                    iconStyle={styles.iconStyle}
                                    data={auditLevels && auditLevels.length > 0 ? auditLevels : []}
                                    search
                                    maxHeight={300}
                                    labelField="label"
                                    valueField="value"
                                    placeholder="Выберите значение"
                                    searchPlaceholder="Поиск..."
                                    value={auditLevelId}
                                    searchField="label"
                                    onChange={item => {
                                        setAuditLevelId(item.value);
                                    }}
                                />
                            </View>
                        </>        
                    ) : (
                        <></>
                    )}
                    <View style={{ marginHorizontal: 10}}>
                        <Text style={styles.detail}>Подразделение:</Text>
                    </View>
                    <View style={{}}>
                        <Dropdown
                            style={styles.dropdown}
                            placeholderStyle={styles.placeholderStyle}
                            selectedTextStyle={styles.selectedTextStyle}
                            inputSearchStyle={styles.inputSearchStyle}
                            itemTextStyle={styles.inputSearchStyle}
                            iconStyle={styles.iconStyle}
                            data={audit.departments &&  audit.departments.length > 0 ? audit.departments : []}
                            search
                            maxHeight={300}
                            labelField="label"
                            valueField="value"
                            placeholder="Выберите значение"
                            searchPlaceholder="Поиск..."
                            value={departmentId}
                            searchField="label"
                            onChange={item => {
                                setDepartmentId(item.value);
                            }}
                        />
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 10, marginHorizontal: 10}}>
                        <View style={{flex: 0.3}}>
                            <Text style={styles.detail}>Дата:</Text>
                        </View>
                        <View style={{flex: 0.3}}>
                            <Text style={styles.title}>{moment(date).format('DD.MM.YYYY')}</Text>
                        </View>
                        <View style={{flex: 0.4}}>
                            <TouchableOpacity 
                                onPress={showDatepicker}
                                style={styles.actions}>
                                <Ionicons name='calendar' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                        <Text style = {styles.userText}>Несоответствия</Text>
                    </View>
                </View> 
                
            }
            ListFooterComponent={() =>
                <View style={{flexDirection: 'row', alignItems: 'center',marginVertical: 10, marginHorizontal: 10}}>
                    <View style={{flex: 0.3}}></View>
                    <View style={{flex: 0.4}}>
                        <Button 
                        
                            title='Сохранить'
                            onPress={() => {

                            
                                let auditResults = {};

                                auditResults.UserId = userInfo.Id;
                                auditResults.AuditTypeId = auditTypeId;
                                auditResults.AuditLevelId = auditLevelId;
                                auditResults.DepartmentId = departmentId;
                                auditResults.Date = date ? moment(date).format('YYYY-MM-DD') : null;
                                auditResults.Contents = auditContents;

                                axios
                                    .post(`${BASE_URL}/saveAudit`, auditResults, {
                                        withCredentials: false,
                                        headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                                    })
                                    .then(res => {
                                        navigation.goBack();
                                    })
                                    .catch(e => {
                                        console.log(e);
                                    })
                            }}
                        />
                    </View>
                    <View style={{flex: 0.2}}></View>
                    <View style={{flex: 0.1}}>
                        <TouchableOpacity onPress={() => {
                                auditContents.push({
                                    id: auditContents.length == 0 ? 1 :  auditContents[auditContents.length-1].id + 1,
                                    content: ''
                                })
                                navigation.navigate('Finding', {array: auditContents, id: auditContents[auditContents.length-1].id})
                            }} style={styles.actions}>
                            <Ionicons name='add-circle' size={24} color='#ff6347' />
                        </TouchableOpacity>
                    </View>
                </View>      
            }     
        />
    );
};

export default AuditScreen;

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderWidth: 0.5,
        padding: 10,
        fontSize: 18,
        color: '#428bca'
      },
    dropdown: {
        margin: 16,
        height: 50,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
      },
      icon: {
        marginRight: 5,
      },
      placeholderStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      selectedTextStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 18,
        color: 'black',
      },
    actions: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#fff',
        borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 5,
        marginVertical: 8,
        marginHorizontal: 10,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
    wrapperSetting: {
        padding: 20,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
      },
      userText: {
        fontSize: 18,
        color: '#428bca',
        textAlign: 'center'
    },  
  });