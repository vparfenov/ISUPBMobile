import React, {useState, useContext, useEffect, useCallback}  from 'react';
import { Text, View, FlatList, StyleSheet, StatusBar, Image, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AuthContext } from '../context/AuthContext';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';

 const CreateScreen = ({navigation}) => {


    const [createList, setCreateList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const onPressSafetyTalk = () => { navigation.navigate('SafetyTalk') }; 
    const onPressAudit = () => { navigation.navigate('Audit') }; 

    const {userInfo} = useContext(AuthContext);  

    
    useEffect(() => {
        getCreateList()
     }, [])


    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            //console.log('focus');
            onRefresh();
      
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                //console.log('unfocus')
              };
        }, [])
    );

    const [needRefresh, setNeedRefresh] = useState(false);  

    const wait = (timeout) => { // Defined the timeout function for testing purpose
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const onRefresh = useCallback(() => {
        setNeedRefresh(true);
        getCreateList();
    }, []);

    const getCreateList = () => {

      setIsLoading(true)

      axios
          .get(`${BASE_URL}/getCreateList`, {
              params: {
                  userId: userInfo.Id,
              },
              withCredentials: false,
              headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
          })
          .then(res => {
              let createList = res.data;
              setCreateList(createList);
              setIsLoading(false);
              setNeedRefresh(false);

          })
          .catch(e => {
              console.log(e);
              setIsLoading(false);
              setNeedRefresh(false);
          })
    }

    return (
        <FlatList
                data={createList}
                refreshing={needRefresh}
                onRefresh={onRefresh}
                renderItem={({item}) => (
                    <View style={styles.item}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{flex: 0.1}}>
                                <Image
                                    source={{
                                      uri: item.uri,
                                      width: 30,
                                      height: 30
                                  }}
                                />
                            </View>   
                            <View style={{flex: 0.9}}>
                                <Text  style={styles.title}>  {item.title} </Text>
                            </View>     
                        </View>
                        {item.type == 1 ? (
                          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
                                <View style={{flexDirection: 'row', flex: 0.9}}>
                                    <View style={{flex: 0.35}}>
                                        <Text  style={styles.detail}>План:</Text>
                                    </View>  
                                    <View style={{flex: 0.15}}>
                                        <Text  style={styles.title}>{item.plan}</Text>
                                    </View>  
                                    <View style={{flex: 0.35}}>
                                        <Text  style={styles.detail}>Факт:</Text>
                                    </View>  
                                    <View style={{flex: 0.15}}>
                                        <Text  style={styles.title}>{item.fact}</Text>
                                    </View>       
                                </View>
                                <View style={{flex: 0.1}}>
                                    <TouchableOpacity onPress={onPressSafetyTalk} style={styles.actions}>
                                        <Ionicons name='add-circle' size={24} color='#ff6347' />
                                    </TouchableOpacity>
                                </View>
                          </View>
                        ) : (
                          <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
                                <View style={{flexDirection: 'row', flex: 0.9}}>
                                    <View style={{flex: 0.35}}>
                                        <Text  style={styles.detail}>Выявлено:</Text>
                                    </View>  
                                    <View style={{flex: 0.15}}>
                                        <Text  style={styles.title}>{item.found}</Text>
                                    </View>  
                                    <View style={{flex: 0.35}}>
                                        <Text  style={styles.detail}>Устранено:</Text>
                                    </View>  
                                    <View style={{flex: 0.15}}>
                                        <Text  style={styles.title}>{item.corrected}</Text>
                                    </View>       
                                </View>
                                <View style={{flex: 0.1}}>
                                    <TouchableOpacity onPress={onPressAudit} style={styles.actions}>
                                        <Ionicons name='add-circle' size={24} color='#ff6347' />
                                    </TouchableOpacity>
                                </View>
                          </View>
                        )}
                        
                    </View>
                )}
                keyExtractor={item => item.id}
            />
    );    
};

const styles = StyleSheet.create({
    actions: {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:30,
      height:30,
      backgroundColor:'#fff',
      borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 12,
        color: 'black'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });

export default CreateScreen;