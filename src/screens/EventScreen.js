import React, {useState, useEffect, useContext, useCallback}  from 'react';
import { Text, View, FlatList, StyleSheet, StatusBar, Image, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';
import { AuthContext } from '../context/AuthContext';
import ReactNativeBlobUtil from 'react-native-blob-util';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

 const EventScreen = ({navigation}) => {

    const [eventList, setEventList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);

    const {userInfo} = useContext(AuthContext);  

    const onPressTest = (objectId) => navigation.navigate('Test', {objectId: objectId});
    const onPressWorkPermit = () => navigation.navigate('WorkPermit');

    useEffect(() => {
        getEventList()
    }, [])

    const getEventList = () => {

        setIsLoading(true)

        axios
            .get(`${BASE_URL}/getEventList`, {
                params: {
                    userId: userInfo.Id,
                },
                withCredentials: false,
                headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
            })
            .then(res => {
                let eventList = res.data;
                setEventList(eventList);
                setIsLoading(false);
                setNeedRefresh(false);

            })
            .catch(e => {
                console.log(e);
                setIsLoading(false);
                setNeedRefresh(false);
            })


    }

    const android = ReactNativeBlobUtil.android;
    const dirs = ReactNativeBlobUtil.fs.dirs;

    const renderItem = ({item}) => {
        return (
            <View style={styles.item}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{flex: 0.1}}>
                        <Image
                            source={{
                                uri: item.uri,
                                width: 30,
                                height: 30
                            }}
                        />
                    </View>   
                    <View style={{flex: 0.9}}>
                        <Text  style={styles.title}>{item.title} </Text>
                    </View>     
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
                     <View style={{flex: 1}}>
                        <Text style={styles.detail}>{item.detail} </Text>
                     </View>
                </View>
                {item.type == 1 ? (
                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
                        <View style={{flex: 0.6}}>
                            <Text style={styles.date}>{item.date} </Text>
                        </View>
                        <View style={{flex: 0.20}}>
                            <TouchableOpacity 
                                onPress={() => {navigation.navigate('Module', {objectId: item.objectId, mode: 1, title: 'Тестирование'}) }} 
                                style={styles.actions}>
                                <Ionicons name='play' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 0.20}}>
                            <TouchableOpacity 
                                onPress={() => {navigation.navigate('Module', {objectId: item.objectId, mode: 2, title: 'Обучение'}) }} 
                                style={styles.actions}>
                                <Ionicons name='flask' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                    </View>          
                ) : (
                    <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 8}}>
                        <View style={{flex: 0.55}}>
                            <Text style={styles.date}>{item.date} </Text>
                        </View>
                        <View style={{flex: 0.15}}>
                            <TouchableOpacity 
                                onPress={() => {
                                    ReactNativeBlobUtil
                                    .config({
                                        fileCache: true,
                                        //path: dirs.DocumentDir + '/workpermit.zip'
                                        appendExt: 'zip',
                                        //addAndroidDownloads: {
                                        //    useDownloadManager: true,
                                        //    title: 'workpermit.zip',   
                                        //    notification: false,
                                        //    mime: 'application/zip',
                                        //    mediaScannable: true,
                                        //    description: 'Файл скачан менеджером загрузок.'
                                        //}
                                    })
                                    .fetch('GET', `${BASE_URL}/getWorkPermitFile/${item.workPermitId}`, {
                                        'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                                    )   
                                    .then((res) => {
                                        android.actionViewIntent(res.path(), 'application/zip')
                                    }) 
                                    .catch((err) => {
                                        console.log(err);
                                    })
        
                                }} 
                                style={styles.actions}>
                                <Ionicons name='cloud-download' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 0.15}}>
                            <TouchableOpacity 
                                onPress={() => {
                                    axios
                                    .put(`${BASE_URL}/saveApprovingDate/${item.objectId}`, {
                                        userId : userInfo.Id,
                                    }, { 
                                        withCredentials: false,
                                        headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                                    })
                                    .then(res => {
                                       onRefresh()
                                    })
                                    .catch(e => {
                                        console.log(e);
                                    })
                                }} 
                                style={styles.actions}>
                                <Ionicons name='checkmark-circle' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: 0.15}}>
                            <TouchableOpacity 
                                onPress={() => {navigation.navigate('Revision', {objectId: item.objectId}) }} 
                                style={styles.actions}>
                                <Ionicons name='arrow-undo-circle' size={24} color='#ff6347' />
                            </TouchableOpacity>
                        </View>
                    </View>    
                )}
                   
            </View>        
        )
    }

    useFocusEffect(
        React.useCallback(() => {
            // Do something when the screen is focused
            //console.log('focus');
            onRefresh();
      
            return () => {
                // Do something when the screen is unfocused
                // Useful for cleanup functions
                //console.log('unfocus')
              };
        }, [])
      );

    const [needRefresh, setNeedRefresh] = useState(false);

    const wait = (timeout) => { // Defined the timeout function for testing purpose
        return new Promise(resolve => setTimeout(resolve, timeout));
    }    

    const onRefresh = useCallback(() => {
        setNeedRefresh(true);
        getEventList();
    }, []);


    return (

        <FlatList
                data={eventList}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                refreshing={needRefresh}
                onRefresh={onRefresh}
            />
          );   
};

const styles = StyleSheet.create({
    actions: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#fff',
        borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });

export default EventScreen;