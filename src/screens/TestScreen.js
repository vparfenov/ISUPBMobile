import React, { useContext } from 'react';
import { Text, View, FlatList, StyleSheet, StatusBar, Button, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CheckBox from '@react-native-community/checkbox';
import _ from 'lodash';


class MyCheckBox extends React.Component {

    state = {
        isChecked: false,
    }

    render() {

        return (
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <CheckBox
                    tintColors={{ true: '#F15927', false: 'black' }}
                    disabled={false}
                    value={this.state.isChecked}
                    onValueChange={(newValue) => {
                      this.props.data.IsSelected = newValue;
                      this.setState({isChecked: newValue});
                    }}
                />    
                <View>
                    {this.props.data.answers.map((answer, i) => {return <View key={i} >
                            <Text  style={styles.detail}>{answer} </Text>
                            {this.props.data.images.length >= (i+1) ? (
                                <View style={{justifyContent: 'center'}}>
                                    <Image
                                        style={{
                                            width: this.props.data.images[i].width,
                                            height: this.props.data.images[i].height,
                                            alignSelf: 'center'
                                        }}
                                        source={{uri: this.props.data.images[i].uri}}
                                    />        
                                </View>
                            ) : (
                                <></>
                            )}
                        </View> 
                    })}
                </View>
            </View>
        );
      }
}


const TestScreen = ({ route, navigation }) => {

    const {module, mode} = route.params;
    const Quizzes = mode == 1 ? module.TestQuizzes : module.ModuleQuizzes;

    _.each(Quizzes, quizz => {

        quizz.images = [];            

        let matchAll = quizz.Question.matchAll(/<img[^>]+>/g);
        
        let imageTags = Array.from(matchAll);

        _.each(imageTags, (imageTag, i) => {

            let image = {};
            
            image.height = imageTag[0].match(/HEIGHT="([^"]+)"/) ? parseInt(imageTag[0].match(/HEIGHT="([^"]+)"/)[1]) : 0;
            image.width = imageTag[0].match(/WIDTH="([^"]+)"/) ? parseInt(imageTag[0].match(/WIDTH="([^"]+)"/)[1]) : 0;
            image.uri = imageTag[0].match(/src="([^"]+)"/) ? imageTag[0].match(/src="([^"]+)"/)[1] : '';
            image.id = i;
            
            if (image.height && image.width && image.uri) {
                quizz.images.push(image)
            }

        })

        quizz.questions = quizz.Question.replace(/\r\n/g,'').replace(/<br>/g,'\r\n').split(/<img[^>]+>/g);
        
        _.each(quizz.Answers, answer => {
            
            answer.IsSelected = false;
            answer.images = [];

            let matchAll = answer.Answer.matchAll(/<img[^>]+>/g);
        
            let imageTags = Array.from(matchAll);

            _.each(imageTags, (imageTag, i) => {

                let image = {};
                
                image.height = imageTag[0].match(/HEIGHT="([^"]+)"/) ? parseInt(imageTag[0].match(/HEIGHT="([^"]+)"/)[1]) : 0;
                image.width = imageTag[0].match(/WIDTH="([^"]+)"/) ? parseInt(imageTag[0].match(/WIDTH="([^"]+)"/)[1]) : 0;
                image.uri = imageTag[0].match(/src="([^"]+)"/) ? imageTag[0].match(/src="([^"]+)"/)[1] : '';
                image.id = i;
                
                if (image.height && image.width && image.uri) {
                    answer.images.push(image)
                }
    
            })

            answer.answers = answer.Answer.replace(/\r\n/g,'').replace(/<br>/g,'\r\n').split(/<img[^>]+>/g);
        })


    })

    return (

        <FlatList
          data={Quizzes}
          renderItem={({item}) => (
            <View style={styles.item}>
                <View>
                    {item.questions.map((question, i) => {return <View key={i} >
                            <Text  style={styles.question}>{question} </Text>
                            {item.images.length >= (i+1) ? (
                                <View style={{justifyContent: 'center'}}>
                                    <Image
                                        style={{
                                            width: item.images[i].width,
                                            height: item.images[i].height,
                                            alignSelf: 'center'
                                        }}
                                        source={{uri: item.images[i].uri}}
                                    />        
                                </View>
                            ) : (
                                <></>
                            )}
                        </View> 
                    })}
                  
                </View>
                <View style={{width: '90%'}}>
                    <FlatList
                      data={item.Answers}
                      renderItem={({item}) => (
                          <MyCheckBox data = {item}> </MyCheckBox>
                      )}
                    >
                      
                    </FlatList>
                </View>
            </View>  
          )}
          keyExtractor={item => item.Id}
          ListHeaderComponent={() =>
            <View style={styles.wrapperSetting}> 
                <Text style = {styles.title}>{module.Name} </Text>
            </View> 
            }
            ListFooterComponent={() =>
                <View style={styles.wrapperSetting}>
                    <Button 
                        title='Завершить'
                        onPress={() => {

                            let properAnswerCount = 0;
                          
                            _.each(Quizzes, quizz => {

                                let correctAnswers = _(quizz.Answers)
                                    .chain()
                                    .filter(el => el.IsTrue)
                                    .map(el => el.Id)
                                    .value();

                                let currentAnswers = _(quizz.Answers)
                                    .chain()
                                    .filter(el => el.IsSelected)
                                    .map(el => el.Id)
                                    .value();


                                if(currentAnswers.length == 0){
                                      return;
                                }
  
                                if(currentAnswers.length != correctAnswers.length){
                                      return;
                                }
  
                                if (_.differenceWith(currentAnswers, correctAnswers, (a, b) => a == b).length == 0) {
                                      properAnswerCount++;
                                }


                            })

                            let percent = Math.floor(properAnswerCount * 100 / Quizzes.length);
                            
                            module.CurrentPercentage = percent;

                            navigation.goBack();

                        }}
                    />
                    
                </View>      
            }       
        >
          
        </FlatList>
        
     )

}


const styles = StyleSheet.create({
    wrapperSetting: {
        padding: 20,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
      },
    title: {
        padding: 0,
        fontSize: 18,
        color: '#428bca'
      },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 12,
        color: 'black'
    },
    question: {
        fontSize: 14,
        color: '#428bca'
      },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });

export default TestScreen