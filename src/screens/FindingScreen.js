import React, {useState} from 'react';
import { ScrollView, TextInput, StyleSheet, StatusBar, View, Button} from 'react-native';
import _ from 'lodash';

const FindingScreen = ({ route, navigation }) => {

    const {array, id} = route.params;

    const [finding, setFinding] = useState(_.find(array, el => el.id == id) ? _.find(array, el => el.id == id).content : '');
    const [inputHeight, setInputHeight] = useState(40);


    return (
        <ScrollView>
            <TextInput
                multiline
                value={finding}
                onChangeText={(newValue) => {
                    setFinding(newValue);
                }}
                onContentSizeChange={event => {
                    setInputHeight(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)))
                }}
                style={[styles.input, { height: inputHeight, padding: 5, marginVertical: 10, marginHorizontal: 10 }]}
                placeholder="Введите данные..."
            >
            </TextInput>
            <View style={{flexDirection: 'row', alignItems: 'center',marginVertical: 10, marginHorizontal: 10}}>
                <View style={{flex: 0.3}}></View>
                <View style={{flex: 0.4}}>
                    <Button 
                        title='Сохранить'
                        onPress={() => {

                            if (_.find(array, el => el.id == id )) {
                                _.find(array, el => el.id == id ).content = finding;
                            }
                            navigation.goBack();

                        }}
                    />
                </View>
                <View style={{flex: 0.3}}></View>
            </View>
        </ScrollView>

    );
};

export default FindingScreen;

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderWidth: 0.5,
        padding: 10,
        fontSize: 18,
        color: '#428bca'
      },
    dropdown: {
        margin: 16,
        height: 50,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
      },
      icon: {
        marginRight: 5,
      },
      placeholderStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      selectedTextStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 18,
        color: 'black',
      },
    actions: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#fff',
        borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
    wrapperSetting: {
        padding: 20,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
      },
      userText: {
        fontSize: 18,
        color: '#428bca',
        textAlign: 'center'
    },  
  });