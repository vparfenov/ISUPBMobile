import React, { useState, useContext,  } from 'react'
import { Text, SafeAreaView, ScrollView, TextInput, View, StyleSheet, StatusBar, Button} from 'react-native';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';
import { AuthContext } from '../context/AuthContext';


const RevisionScreen = ({ route, navigation  }) => {

    const {objectId} = route.params;

    const [isLoading, setIsLoading] = useState(false);

    const {userInfo} = useContext(AuthContext);  

    const [coment, setComent] = useState('');
    const [inputHeight, setInputHeight] = useState(40);
   

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Комментарий:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <ScrollView>
                        <TextInput
                            multiline
                            value={coment}
                            onChangeText={setComent}
                            onContentSizeChange={event => {
                                setInputHeight(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)));
                            }}
                            style={[styles.input, { height: inputHeight }]}
                            placeholder="Введите данные..."
                        >
                        </TextInput>
                    </ScrollView>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Button 
                        title='Отправить'
                        onPress={() => {
                            axios
                            .put(`${BASE_URL}/saveApprovingComment`, {
                                userId: userInfo.Id,
                                Id: objectId,
                                Comment: coment,
                            }, {
                                withCredentials: false,
                                headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                            })
                            .then(res => {
                                navigation.goBack();
                            })
                            .catch(e => {
                                console.log(e);
                            })
                            
                        }}
                    />
                </View>
            </ScrollView>    
        </SafeAreaView>    

    );
};

export default RevisionScreen;

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderWidth: 0.5,
        padding: 10,
        fontSize: 18,
        color: '#428bca'
      },
    dropdown: {
        margin: 16,
        height: 50,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
      },
      icon: {
        marginRight: 5,
      },
      placeholderStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      selectedTextStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 18,
      },
    actions: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#fff',
        borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });