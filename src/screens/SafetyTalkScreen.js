import React, { useState, useContext, useEffect } from 'react'
import { Text, SafeAreaView, ScrollView, TextInput, View, StyleSheet, StatusBar, TouchableOpacity, Button} from 'react-native';
import moment from 'moment';
import { DateTimePickerAndroid } from '@react-native-community/datetimepicker';
import { Dropdown } from 'react-native-element-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CheckBox from '@react-native-community/checkbox';
import RadioGroup from 'react-native-radio-buttons-group';
import { Rating } from 'react-native-ratings';
import axios from 'axios';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';
import { AuthContext } from '../context/AuthContext';
import _ from 'lodash';

class MyCheckBox extends React.Component {

    state = {
        isChecked: false,
        selectedId: null,
        description: '',
        inputHeight: 40,
    }

    render() {

        return (
            <View>
                <View style={{flexDirection: 'row', alignItems: 'flex-start'}}>
                    <CheckBox
                        tintColors={{ true: '#F15927', false: 'black' }}
                        disabled={false}
                        value={this.state.isChecked}
                        onValueChange={(newValue) => {
                            this.props.data.isSelected = newValue;
                            if (!newValue) {
                                this.props.data.observationCategoryId = null;
                                this.props.data.description = '';
                                this.setState({selectedId: null});
                                this.setState({description: ''});
                            }
                            this.setState({isChecked: newValue});
                        }}
                    />    
                    <Text style={styles.detail}> {this.props.data.name} </Text>
                </View>
                <View>
                    {this.props.data.isSelected ? (
                        <RadioGroup 
                            containerStyle={{ alignSelf: 'flex-start', alignItems: 'left'}}
                            labelStyle={{color: 'black'}}
                            radioButtons={this.props.data.observationCategories} 
                            onPress={(newValue) => {
                                this.props.data.observationCategoryId = parseInt(newValue);
                                this.setState({selectedId: newValue});
                            }}
                            selectedId={this.state.selectedId}
                    /> 
                    ) : (
                        <></>
                    )}
                    
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    {this.props.data.isSelected ? (
                        <ScrollView>
                            <TextInput
                                multiline
                                value={this.state.description}
                                onChangeText={(newValue) => {
                                    this.props.data.description = newValue;
                                    this.setState({description: newValue});
                                }}
                                onContentSizeChange={event => {
                                    this.setState({inputHeight: Math.min(80, Math.max(40, event.nativeEvent.contentSize.height))})
                                }}
                                style={[styles.input, { height: this.state.inputHeight }]}
                                placeholder="Введите данные..."
                            >
                            </TextInput>
                        </ScrollView>
                    ) : (
                        <></>
                    )}
                    
                </View>     
            </View>
        );
      }
}

const SafetyTalkScreen = ({ navigation }) => {


    const [safetyTalk, setSafetyTalk] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const {userInfo} = useContext(AuthContext);  

    useEffect(() => {
        getSafetyTalk();
        //console.log(safetyTalk);
    }, [])

    
    const getSafetyTalk = () => {

        setIsLoading(true)

        return axios
            .get(`${BASE_URL}/getSafetyTalk`, {
                params: {
                    userId: userInfo.Id,
                },
                withCredentials: false,
                headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
            })
            .then(res => {
                //console.log(safetyTalk);
                setSafetyTalk(res.data);
                setIsLoading(false);
                //console.log(res.data);

            })
            .catch(e => {
                console.log(e);
                setIsLoading(false);
            })


    }


    const [date, setDate] = useState(moment().toDate());
    const [departmentId, setDepartmentId] = useState(null);
    const [location, setLocation] = useState('');
    const [employeeOneName, setEmployeeOneName] = useState('');
    const [duration, setDuration] = useState('');
    const [grade, setGrade] = useState(null);
    const [positive, setPositive] = useState('');
    const [inputHeight, setInputHeight] = useState(40);
    const [employeeOneId, setEmployeeOneId] = useState(null);
    const [corrected, setCorrected] = useState('');
    const [inputHeight1, setInputHeight1] = useState(40);
    const [fixLater, setFixLater] = useState('');
    const [inputHeight2, setInputHeight2] = useState(40);
    const [agreements, setAgreements] = useState('');
    const [inputHeight3, setInputHeight3] = useState(40);
    const [fixDate, setFixDate] = useState('');
    const [gradeEmployee, setGradeEmployee] = useState(3);
    const [gradeEmployeeOne, setGradeEmployeeOne] = useState(3);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate;
        setDate(currentDate);
      };
    
    const showMode = (currentMode) => {
        DateTimePickerAndroid.open({
          value: date,
          onChange,
          mode: currentMode,
          is24Hour: true,
          locale: "ru-RU"
        });
    };
    
    const showDatepicker = () => {
        showMode('date');
    };

    const showModeFixDate = (currentMode) => {
        DateTimePickerAndroid.open({
          value: fixDate || moment().toDate(),
          onChange: onChangeFixDate,
          mode: currentMode,
          is24Hour: true,
          locale: "ru-RU"
        });
    };

    const onChangeFixDate = (event, selectedDate) => {
        const currentDate = selectedDate;
        setFixDate(currentDate);
    };

    const showDatepickerFixDate = () => {
        showModeFixDate('date');
    };
    

    return (
        <SafeAreaView>
            <ScrollView>
                <View style={{flexDirection: 'row', alignItems: 'center', padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <View style={{flex: 0.3}}>
                        <Text style={styles.detail}>Дата:</Text>
                    </View>
                    <View style={{flex: 0.3}}>
                        <Text style={styles.title}>{moment(date).format('DD.MM.YYYY')}</Text>
                    </View>
                    <View style={{flex: 0.4}}>
                        <TouchableOpacity 
                                onPress={showDatepicker}
                                style={styles.actions}>
                                <Ionicons name='calendar' size={24} color='#ff6347' />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Подразделение где проводилась беседа:</Text>
                </View>
                <View style={{padding: 5,}}>
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        itemTextStyle={styles.inputSearchStyle}
                        data={safetyTalk.departments &&  safetyTalk.departments.length > 0 ? safetyTalk.departments : []}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Выберите значение"
                        searchPlaceholder="Поиск..."
                        value={departmentId}
                        searchField="label"
                        onChange={item => {
                            setDepartmentId(item.value)
                        }}
                    />
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Место проведения беседы:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <TextInput
                        style={styles.input}
                        onChangeText={setLocation}
                        value={location}
                        placeholder="Введите место проведения..."
                    >
                    </TextInput>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>С кем проводилась беседа:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <TextInput
                        style={styles.input}
                        onChangeText={setEmployeeOneName}
                        value={employeeOneName}
                        placeholder="Введите имя..."
                    >
                    </TextInput>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <View style={{flex: 0.7}}>
                        <Text style={styles.detail}>Продолжительность беседы:</Text>
                    </View>    
                    <View style={{flex: 0.3}}>
                        <TextInput
                            style={styles.input}
                            onChangeText={setDuration}
                            value={duration}
                            placeholder="0..."
                            keyboardType="numeric"
                        >
                        </TextInput>
                    </View>  
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Характер беседы:</Text>
                </View>
                <View style={{padding: 5,}}>
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        itemTextStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={safetyTalk.grades &&  safetyTalk.grades.length > 0 ? safetyTalk.grades : []}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Выберите значение"
                        searchPlaceholder="Поиск..."
                        value={grade}
                        searchField="label"
                        onChange={item => {
                            setGrade(item.value)
                        }}
                    />        
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Позитивное условие / действие выявленное в ходе наблюдения:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <ScrollView>
                        <TextInput
                            multiline
                            value={positive}
                            onChangeText={setPositive}
                            onContentSizeChange={event => {
                                setInputHeight(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)));
                            }}
                            style={[styles.input, { height: inputHeight }]}
                            placeholder="Введите данные..."
                        >
                        </TextInput>
                    </ScrollView>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Кому выдана СОТА:</Text>
                </View>
                <View style={{padding: 5,}}>
                    <Dropdown
                        style={styles.dropdown}
                        placeholderStyle={styles.placeholderStyle}
                        selectedTextStyle={styles.selectedTextStyle}
                        inputSearchStyle={styles.inputSearchStyle}
                        itemTextStyle={styles.inputSearchStyle}
                        iconStyle={styles.iconStyle}
                        data={safetyTalk.employeeOnes &&  safetyTalk.employeeOnes.length > 0 ? safetyTalk.employeeOnes : []}
                        search
                        maxHeight={300}
                        labelField="label"
                        valueField="value"
                        placeholder="Выберите значение"
                        searchPlaceholder="Поиск..."
                        value={employeeOneId}
                        searchField="label"
                        onChange={item => {
                            setEmployeeOneId(item.value)
                        }}
                    />            
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Темы беседы:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    {safetyTalk.conversationTopics && safetyTalk.conversationTopics.length > 0 ? (
                        safetyTalk.conversationTopics.map(item => {
                            return <View key={item.id}>
                                <MyCheckBox data = {item}> </MyCheckBox>
                        </View>
                        })    
                    ) : (
                        <></>
                    )}  
                </View>  
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Результаты беседы:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Исправлено во время беседы:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <ScrollView>
                        <TextInput
                            multiline
                            value={corrected}
                            onChangeText={setCorrected}
                            onContentSizeChange={event => {
                                setInputHeight1(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)));
                            }}
                            style={[styles.input, { height: inputHeight1 }]}
                            placeholder="Введите данные..."
                        >
                        </TextInput>
                    </ScrollView>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Договоренность исправить позднее:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <ScrollView>
                        <TextInput
                            multiline
                            value={fixLater}
                            onChangeText={setFixLater}
                            onContentSizeChange={event => {
                                setInputHeight2(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)));
                            }}
                            style={[styles.input, { height: inputHeight2 }]}
                            placeholder="Введите данные..."
                        >
                        </TextInput>
                    </ScrollView>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Достигнутые договоренности по итогам беседы:</Text>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <ScrollView>
                        <TextInput
                            multiline
                            value={agreements}
                            onChangeText={setAgreements}
                            onContentSizeChange={event => {
                                setInputHeight3(Math.min(80, Math.max(40, event.nativeEvent.contentSize.height)));
                            }}
                            style={[styles.input, { height: inputHeight3 }]}
                            placeholder="Введите данные..."
                        >
                        </TextInput>
                    </ScrollView>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <View style={{flex: 0.3}}>
                        <Text style={styles.detail}>Дата проверки:</Text>
                    </View>
                    <View style={{flex: 0.3}}>
                        <Text style={styles.title}>{fixDate ? moment(fixDate).format('DD.MM.YYYY') : ''}</Text>
                    </View>
                    <View style={{flex: 0.4}}>
                        <TouchableOpacity 
                            onPress={showDatepickerFixDate}
                            style={styles.actions}>
                            <Ionicons name='calendar' size={24} color='#ff6347' />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Оценка реакции сотрудника на беседу:</Text>
                </View>
                <View style={{ padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Rating
                        type='custom'
                        ratingColor='tomato'
                        ratingBackgroundColor='lightgray'
                        tintColor='whitesmoke'
                        startingValue={gradeEmployee}
                        fractions={1}
                        jumpValue={0.5}
                        onFinishRating={(rating) => {
                           setGradeEmployee(rating);
                        }}
                    />
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Text style={styles.detail}>Впечатление проводившего беседу:</Text>
                </View>
                <View style={{ padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Rating
                        type='custom'
                        ratingColor='tomato'
                        tintColor='whitesmoke'
                        ratingBackgroundColor='lightgray'
                        startingValue={gradeEmployeeOne}
                        fractions={1}
                        jumpValue={0.5}
                        onFinishRating={(rating) => {
                            setGradeEmployeeOne(rating);
                        }}
                    />
                </View>
                <View style={{padding: 5, marginVertical: 10, marginHorizontal: 10}}>
                    <Button 
                        title='Зафиксировать'
                        onPress={() => {



                            let safetyTalkResult = {};

                            safetyTalkResult.UserId = userInfo.Id;
                            safetyTalkResult.Date = date ? moment(date).format('YYYY-MM-DD') : null;
                            safetyTalkResult.DepartmentId = departmentId ? parseInt(departmentId) : null;
                            safetyTalkResult.Location = location || null;
                            safetyTalkResult.EmployeeOneName = employeeOneName || null;
                            safetyTalkResult.Duration = duration ? parseInt(duration) : null;
                            safetyTalkResult.Grade = grade ? 1 : 0;
                            safetyTalkResult.Positive = positive || null;
                            safetyTalkResult.EmployeeOneId = employeeOneId ? parseInt(employeeOneId) : null;
                            safetyTalkResult.IsSotaGiven = employeeOneId ? 1 : 0;
                            safetyTalkResult.contents = _(safetyTalk.conversationTopics)
                                .chain()
                                .map(el => {return {
                                    StConversationTopicId: el.id,
                                    IsSelected: el.isSelected,
                                    ObservationCategoryId: el.observationCategoryId,
                                    Description: el.description

                                }})
                                .value();
                            safetyTalkResult.Corrected = corrected || null;
                            safetyTalkResult.FixLater = fixLater || null;
                            safetyTalkResult.Agreements = agreements || null
                            safetyTalkResult.FixDate = fixDate ? moment(fixDate).format('YYYY-MM-DD') : null;
                            safetyTalkResult.GradeEmployee = gradeEmployee ? parseFloat(gradeEmployee) : null;
                            safetyTalkResult.GradeEmployeeOne = gradeEmployeeOne ? parseFloat(gradeEmployeeOne) : null;
                            safetyTalkResult.IsClosed = 1;


                            axios
                                .post(`${BASE_URL}/saveSafetyTalk`, safetyTalkResult, {
                                    withCredentials: false,
                                    headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
                                })
                                .then(res => {
                                    navigation.goBack();
                                })
                                .catch(e => {
                                    console.log(e);
                                })
                        }}
                    />
                </View>
            </ScrollView>    
        </SafeAreaView>    

    );
};

export default SafetyTalkScreen;

const styles = StyleSheet.create({
    input: {
        height: 40,
        borderWidth: 0.5,
        padding: 10,
        fontSize: 18,
        color: '#428bca'
      },
    dropdown: {
        margin: 16,
        height: 50,
        borderBottomColor: 'gray',
        borderBottomWidth: 0.5,
      },
      icon: {
        marginRight: 5,
      },
      placeholderStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      selectedTextStyle: {
        fontSize: 18,
        color: '#428bca'
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 18,
        color: 'black',
      },
    actions: {
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        width:30,
        height:30,
        backgroundColor:'#fff',
        borderRadius:50,
    },
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 2},
        shadowRadius: 4,
        shadowOpacity: 0.26,
        elevation: 4,
    },
    title: {
      fontSize: 18,
      color: '#428bca'
    },
    date: {
        fontSize: 18,
        color: '#428bca'
    },
    detail: {
        textAlign: 'left', 
        fontSize: 14,
        color: 'black',
    },
    detail1: {
      textAlign: 'left', 
      fontSize: 14,
      color: 'black',
      padding: 10,
    },
    title1: {
      fontSize: 18,
      color: '#428bca',
      padding: 10,
    },
  });