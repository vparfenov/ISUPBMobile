import React, {useState, useContext} from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import { AuthContext } from '../context/AuthContext';
import Spinner from 'react-native-loading-spinner-overlay';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';

const LoginScreen = () => {

    const debugLogin = false;
    const [server, setServer] = useState(BASE_URL);
    const [kodeks, setKodeks] = useState(KODEKS_VAL);
    const [kodeksData, setKodeksData] = useState(KODEKS_DATA_VAL);
    const [userName, setUserName] = useState(null);
    const [userPass, setUserPass] = useState(null);
    const {isLoading, userInfo, login, logout} = useContext(AuthContext);    

    return (
        <View style = {styles.containerLogin}>
            <Spinner visible = {isLoading}/>
            <View  style = {styles.wrapperLogin}>
                {debugLogin ? (
                    <>
                        <TextInput 
                            style = {styles.inputLogin} 
                            value={server}
                            placeholder='Сервер'
                            placeholderTextColor = 'grey' 
                            onChangeText={text => setServer(text)}
                        />
                        <TextInput 
                            style = {styles.inputLogin} 
                            value={kodeks}
                            placeholder='Kodeks'
                            placeholderTextColor = 'grey' 
                            onChangeText={text => setKodeks(text)}
                        />
                        <TextInput 
                            style = {styles.inputLogin} 
                            value={kodeksData}
                            placeholder='KodeksData'
                            placeholderTextColor = 'grey' 
                            onChangeText={text => setKodeksData(text)}
                        />
                    </>            
                ) : (
                    <></>        
                )}
                <TextInput 
                    style = {styles.inputLogin} 
                    value={userName}
                    placeholder='Имя пользователя'
                    placeholderTextColor = 'grey' 
                    onChangeText={text => setUserName(text)}
                />
                <TextInput 
                    style = {styles.inputLogin} 
                    value={userPass}
                    placeholder='Пароль' secureTextEntry
                    placeholderTextColor = 'grey' 
                    onChangeText={text => setUserPass(text)}
                />  
                <Button 
                    title='Войти'
                    onPress={() => {
                        login(userName, userPass, server, kodeks, kodeksData)
                    }}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    containerLogin: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    wrapperLogin: {
      width: '80%'
    },
    inputLogin: {
      marginBottom: 12,
      borderWidth: 1,
      borderColor: '#bbb',
      borderRadius: 5,
      paddingHorizontal: 14,
      color: 'black',
    }
  });

export default LoginScreen;