import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import React, {createContext, useState}  from 'react';
import {Alert} from 'react-native';
import base64 from 'react-native-base64'
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';



export const AuthContext = createContext({
    isLoading: false,
    userInfo: {},
    login: () => {},
    logout: () => {}
});

export const AuthProvider = ({ children }) => {
    
    const [userInfo, setUserInfo] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const login = (user, password, server, kodeks, kodeksData) => {
        

        setIsLoading(true);

        let authStr = base64.encode(user.toLowerCase() + ':' + password);
             
        
        axios
            //.get(`${server}`, {
            //    params: {
            //        user: user,
            //        password: password
            //    },
            //    withCredentials: false,
            //    headers: {'Cookie': `Auth=${authStr}; Kodeks=${kodeks}; KodeksData=${kodeksData}`}         
            //})
            .get(`${BASE_URL}`, {
                params: {
                    user: user,
                    password: password
                },
                withCredentials: false,
                headers: {'Cookie': `Auth=${authStr}; Kodeks=${KODEKS_VAL}; KodeksData=${KODEKS_DATA_VAL}`}         
            })
            .then(res => {
                //console.log(res.status);
                //console.log(res.headers);
                let userInfo = res.data;
                userInfo.authStr = authStr;
                setUserInfo(userInfo);
                AsyncStorage.setItem('userInfo', JSON.stringify(userInfo));
                setIsLoading(false);

            })
            .catch(e => {
                //if (e.response) {
                //    console.log(e.response.status);
                //    console.log(e.response.headers);
                //    console.log(e.response.data);
                //}
                //if (e.request) {
                //    console.log(e.request);
                //}
                Alert.alert('Ошибка', JSON.stringify(e), [{text: 'Ок', onPress: () => console.log(e),style: 'cancel' }]);
                setIsLoading(false);
            })
        
            
    
    }

    const logout = () => {

        setIsLoading(true);
        AsyncStorage.removeItem('userInfo');
        setUserInfo({});
        setIsLoading(false);

    }

    const value = {
        isLoading: isLoading,
        userInfo: userInfo,
        login: login,
        logout: logout,
      };

    return (
        <AuthContext.Provider value = {value}>{ children }</AuthContext.Provider>
    );
};




