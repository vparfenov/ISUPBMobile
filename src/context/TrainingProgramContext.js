import axios from 'axios';
import React, {createContext, useState, useContext }  from 'react';
import {BASE_URL, KODEKS_VAL, KODEKS_DATA_VAL} from '../config';
import { AuthContext } from '../context/AuthContext';
import _ from 'lodash';
import moment from 'moment';

export const TrainingProgramContext = createContext({
    isLoading: false,
    trainingProgram: {},
    getTrainingProgram: () => {},
    setCurrentPercentage: () => {}
});

export const TrainingProgramProvider = ({ children }) => {

    const [trainingProgram, setTrainingProgram] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    const {userInfo} = useContext(AuthContext);  

    const getTrainingProgram = (objectId) => {

        setIsLoading(true)

        axios
            .get(`${BASE_URL}/getTrainingProgram`, {
                params: {
                    trainingScheduleId : objectId,
                },
                withCredentials: false,
                headers: {'Cookie': `Kodeks=${KODEKS_VAL};  Auth=${userInfo.authStr}; KodeksData=${KODEKS_DATA_VAL}` }          
            })
            .then(res => {
                let data = res.data;

                _.each(data.Modules, module => {

                    module.TestQuizzes = _(module.ModuleQuizzes)
                    .chain()
                    .sampleSize(module.NumberQuestionsTicket)    
                    .value();    

                })

                data.StartTesting = moment().format('YYYY-MM-DD HH:mm:ss');
                
                setTrainingProgram(data);
                setIsLoading(false);
            })
            .catch(e => {
                console.log(e);
                setIsLoading(false);
            })


    }

    const value = {
        isLoading: isLoading,
        trainingProgram: trainingProgram,
        getTrainingProgram: getTrainingProgram,
      };

    return (
        <TrainingProgramContext.Provider value = {value}>{ children }</TrainingProgramContext.Provider>
    );
};